package day1;

public class Lab6 {

	public static void main(String[] args) {
		  Quest1();
	      System.out.println(" ");
	      Quest2();
	      System.out.println(" ");
	      Quest3();
	      
	  }
public static void Quest1() {
	 int sum = 0;
	    for(int i = 1; i <= 10; i++) {
	    	 sum += i;
	      System.out.println("i :" + i);   
	    }
	    System.out.println("sum :" + sum);

}
public static void Quest2() {
		   int x = 1;
		   while (x <= 100) {
		     if(x%12 == 0)
		     {
		    	 System.out.println(x);
		    	 
		     }
		     x++;
		   }
		 }
public static void Quest3() {
	 int newArray[] = {1,2,3,4,5};
	   for(int x : newArray) {
	     System.out.println("x :" + x);
	   }
	 }

	
}
