package day1;

public class Lab9 {

	public static void main(String args[]) {
        for (int number = 1; number <= 20; number++) {
            if (number == 11) // ถ้า count มีค่าเป็น 11 คำสั่ง continue ภายใน if จะทำงาน
                continue; // เริ่มต้นรอบใหม่โดยไม่สนใจคำสั่งที่เหลือด้านล่าง
                System.out.printf("%d ", number); // แสดงค่าตัวแปร count ออกหน้าจอ
        }
        System.out.println("\nUsed continue to skip printing 11");
    }

	
}
