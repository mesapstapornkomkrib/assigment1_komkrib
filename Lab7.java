package day1;

import java.util.Scanner;

public class Lab7 {
	 public static void main(String[] args) {
		 			Quest1();
		 			System.out.println(" ");
		 			Quest2();

	    }
	 public static void Quest1() {
		 int count = 0;
		   do {
		     System.out.println("Count : " + count);
		     count++;
		   } while (count <= 5);
		 
	 }
	 public static void Quest2() {
		 Scanner reader  = new Scanner(System.in);
	        int number;

	        System.out.println("Set odd/even");

	        do {
	            System.out.print("Enter odd number to exit loop: ");
	            number = reader.nextInt();

	            if (number % 2 == 0) {
	                System.out.println("You entered " + number + ", it's even.");
	            } else {
	                System.out.println("You entered " + number + ", it's odd.");
	            }

	        } while (number % 2 == 0);

	        System.out.println("Exited loop.");
	 }

}
